
public class Grid implements Cloneable{
	private Square[][] grid;
	private int size;
	boolean solved;
	private int numBomb;
	public Grid(int size, int numBomb) {
		this.size = size;
		this.numBomb = numBomb;
		grid = new Square[size][size];
		//initializes all the Squares
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				grid[i][j] = new Square(false, i, j);
			}
		}
		//sets bombs in random locations, tries again if random spot is already bomb
		for (int i=0;i<numBomb;i++) {
			while(true) {
				int randX = (int)(Math.random()*size);
	        	int randY = (int)(Math.random()*size);
	        	
	        	if (!grid[randX][randY].isBomb()) {
	        		grid[randX][randY].setBomb(true);
	        		break;
	        	}
			}
			
		}
		//sets value of neighborBombs
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				if (grid[i][j].isBomb()) {
					continue;
				}
				int count = 0;
				for(int a=i-1;a<i+2;a++){
					for(int b=j-1;b<j+2;b++){
						if(isInBounds(a,b) && grid[a][b].isBomb())
						{count++;}
					}
				}
				grid[i][j].setNeighborBombs(count);
				
			}
		}
	}
	//taken from basicagent to simplify code @line39
	public boolean isInBounds(int i, int j) {
		if (i >=0 && i < size && j >=0 && j < size) {
			return true;
		}
		return false;
	}
	public boolean revealSquare(int x, int y) {
		if (grid[x][y].isRevealed()||grid[x][y].isFlagged()) {
			return false;
		}
		grid[x][y].reveal();
		
		if (grid[x][y].isBomb()) {
			//not sure if this line is necessary
			// grid[x][y].setBomb(true);
			System.out.println("UH OH! THAT WAS A MINE!");
		}
		//System.out.println();
		//checking to see if the whole board is solved?
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				if (!grid[i][j].isRevealed()) {
					return true;
				}
			}
		}
		// solved = true;
		// System.out.println("This is complete");
		// print();
		// System.out.println();
		return true;
		
	}
	
	public int getSize() {
		return size;
	}
	public void print() {
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				if (grid[i][j].isFlagged()){
					System.out.print("F");
				}else if (!grid[i][j].isRevealed()) {
					System.out.print("?");
				}else if (grid[i][j].isBomb()){
					System.out.print("B");
				}else {
					System.out.print(grid[i][j].getNeighborBombs());
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public boolean isSolved() {
		return solved;
	}
	public void setSolved(boolean solved) {
		this.solved = solved;
	}
	public Square getSquare(int x, int y) {
		return grid[x][y];
	}
	//Clones object, hopefully works better than manually cloning it
	public Object clone()throws CloneNotSupportedException{  
		return (Grid)super.clone();  
	   }

	public int getNumBomb() {
		return numBomb;
	}

	public void setNumBomb(int numBomb) {
		this.numBomb = numBomb;
	}
	
}
