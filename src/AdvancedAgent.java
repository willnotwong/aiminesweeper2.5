import java.util.LinkedList;
import java.util.Queue;

public class AdvancedAgent {
	private Grid grid;
	private int size;
	Queue<Square> queue = new LinkedList<>();
	public AdvancedAgent(Grid grid) {
		//try catch for the cloning
		this.size = grid.getSize();
		try {
			this.grid = (Grid) grid.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
	}
	public boolean isInBounds(int i, int j) {
		if (i >=0 && i < size && j >=0 && j < size) {
			return true;
		}
		return false;
	}
	//whenever a square is flagged or revealed, call updatebase to make sure every square has updated information
	public void updateBase(Square square) {
		int i = square.getX();
		int j = square.getY();
		int bombCount = 0;
		int hiddenCount = 0;
		int safeCount = 0;
		int neighborCount = 0;
		//from here to line 63 setting up / updating all the information for given square
		//number of identified bombs surrounding the square, identified being either flagged or a revealed bomb
		for(int a=i-1;a<i+2;a++){
			for(int b=j-1;b<j+2;b++){
				if(isInBounds(a,b) && (grid.getSquare(a,b).isFlagged() || (grid.getSquare(a,b).isRevealed() && grid.getSquare(a,b).isBomb())))
				{bombCount++;}
			}
		}
		//hidden squares
		for(int a=i-1;a<i+2;a++){
			for(int b=j-1;b<j+2;b++){
				if(isInBounds(a,b) && !grid.getSquare(a,b).isRevealed())
				{hiddenCount++;}
			}
		}
		//number of identified safe squares
		for(int a=i-1;a<i+2;a++){
			for(int b=j-1;b<j+2;b++){
				if(isInBounds(a,b) && grid.getSquare(a,b).isRevealed() && !grid.getSquare(a,b).isBomb() && !grid.getSquare(i,j).isFlagged())
				{safeCount++;}
			}
		}
		for(int a=i-1;a<i+2;a++){
			for(int b=j-1;b<j+2;b++){
				if(isInBounds(a,b))
				{neighborCount++;}
			}
		}
		grid.getSquare(i,j).setIdentifiedNeighborMines(bombCount);
		grid.getSquare(i,j).setIdentifiedSafeSquares(safeCount);
		grid.getSquare(i,j).setHiddenSquares(hiddenCount);
		grid.getSquare(i,j).setSafeSquares(neighborCount - grid.getSquare(i,j).getIdentifiedNeighborMines());
	}
	//attempts to extract new information, will return false if nothing can be extracted (subsequently calling for random guess)
	public boolean attempt1(Square square){
		boolean newinfo = false;
		int i = square.getX();
		int j = square.getY();
	//if remaining hidden squares are all bombs, flags all hidden squares	
		if (grid.getSquare(i,j).isRevealed()&& !grid.getSquare(i,j).isBomb() && grid.getSquare(i,j).getNeighborBombs() - grid.getSquare(i,j).getIdentifiedNeighborMines() == grid.getSquare(i,j).getHiddenSquares()) {
			for(int a=i-1;a<i+2;a++){
				for(int b=j-1;b<j+2;b++){
					if(isInBounds(a,b) && !grid.getSquare(a,b).isRevealed()){
						grid.getSquare(a,b).reveal();
						grid.getSquare(a,b).setFlagged(true);
						System.out.println("set flag at "+a+" "+b+" because of "+i+" "+j+" loop 1");
						grid.print();
						newinfo=true;
					}
				}
			}
			if(newinfo){
				return true;
			}
		}	
		//if remaining hidden squares are all safe, reveal all squares in question
		if (grid.getSquare(i,j).isRevealed() && !grid.getSquare(i,j).isBomb() && grid.getSquare(i,j).getIdentifiedNeighborMines()==grid.getSquare(i,j).getNeighborBombs()) {
			for(int a=i-1;a<i+2;a++){
				for(int b=j-1;b<j+2;b++){
					if(isInBounds(a,b) && !grid.getSquare(a,b).isBomb() && !grid.getSquare(a,b).isFlagged() && !grid.getSquare(a,b).isRevealed()){
						System.out.println("revealing: "+a+" "+b+" due to square "+i+" "+j);
						grid.revealSquare(a,b);
						grid.print();
						newinfo=true;
					}
				}
			}
			if(newinfo){
				return true;
			}
		}
		return false;
	}
	public boolean attempt2(Square square){
		int i = square.getX();
		int j = square.getY();
		//1-1 pattern
		if((grid.getSquare(i,j).getNeighborBombs() - grid.getSquare(i,j).getIdentifiedNeighborMines() == 1) && (grid.getSquare(i,j).getHiddenSquares() == 2)){
			System.out.println("Potential 1-1 @"+i+" "+j);
			for(int a=i-1;a<i+2;a++){
				for(int b=j-1;b<j+2;b++){
					//making sure we skip diagonals
					if((a==i-1 && b==j-1)||(a==i-1 && b==j+1) || (a==i+1 && b==j-1) || (a==i+1 && b==j+1)){
						continue;
					}
					if(isInBounds(a, b) && grid.getSquare(a,b).getNeighborBombs()-grid.getSquare(a,b).getIdentifiedNeighborMines() == 1 && grid.getSquare(a,b).getHiddenSquares() == 3){
						if((i-a)!=0){
							if(isInBounds(i, j-1) && grid.getSquare(i,j-1).isRevealed()){
								if(isInBounds(i-2*(i-a),j+1) && !grid.getSquare((i-2*(i-a)),(j+1)).isRevealed() && !grid.getSquare((i-2*(i-a)),(j+1)).isBomb()){
									if(!grid.getSquare(a,b+1).isRevealed() && !grid.getSquare(i,j+1).isRevealed()){
										System.out.println("a 1-1 found thanks to "+a+" "+b+", revealing "+(i-2*(i-a))+" "+(j+1));
										grid.revealSquare(i-2*(i-a), j+1);
										grid.print();
										return true;
									}
								}
							}							
							else{
								if(isInBounds(i-2*(i-a),j-1) && !grid.getSquare((i-2*(i-a)),(j-1)).isBomb() && !grid.getSquare((i-2*(i-a)),(j-1)).isRevealed()){
									if(!grid.getSquare(a,b-1).isRevealed() && !grid.getSquare(i,j-1).isRevealed()){
										System.out.println("b 1-1 found thanks to "+a+" "+b+", revealing "+(i-2*(i-a))+" "+(j-1));
										grid.revealSquare(i-2*(i-a),j-1);
										grid.print();
										return true;
									}
								}
							}
						}
						else{
							if(isInBounds(i-1, j) && grid.getSquare(i-1,j).isRevealed()){
								if(isInBounds(i+1,j-2*(j-b)) && !grid.getSquare(i+1,j-2*(j-b)).isBomb() && !grid.getSquare(i+1,j-2*(j-b)).isRevealed()){
									if(!grid.getSquare(a+1,b).isRevealed() && !grid.getSquare(i+1,j).isRevealed()){
										System.out.println("c 1-1 found thanks to "+a+" "+b+", revealing "+(i+1)+" "+(j-2*(j-b)));
										grid.revealSquare(i+1,j-2*(j-b));
										grid.print();
										return true;
									}
								}
							}
							else{
								if(isInBounds(i-1,j-2*(j-b)) && !grid.getSquare(i-1,j-2*(j-b)).isBomb() && !grid.getSquare(i-1,j-2*(j-b)).isRevealed()){
									if(!grid.getSquare(a-1,b).isRevealed() && !grid.getSquare(i-1,j).isRevealed()){
										System.out.println("d 1-1 found thanks to "+a+" "+b+", revealing "+(i-1)+" "+(j-2*(j-b)));
										grid.revealSquare(i-1, j-2*(j-b));
										grid.print();
										return true;
									}
								}
							}
						}						
					}
				}
			}
		}
		//1-2 pattern
		if((grid.getSquare(i,j).getNeighborBombs() - grid.getSquare(i,j).getIdentifiedNeighborMines() == 1) && (grid.getSquare(i,j).getHiddenSquares() == 2 || grid.getSquare(i,j).getHiddenSquares() == 3 || grid.getSquare(i,j).getHiddenSquares() == 4 || grid.getSquare(i,j).getHiddenSquares() == 5)){
			System.out.println("Potential 1-2 @"+i+" "+j);
			for(int a=i-1;a<i+2;a++){
				for(int b=j-1;b<j+2;b++){
					if((a==(i-1) && b==(j-1))||(a==(i-1) && b==(j+1)) || (a==i+1 && b==j-1) || (a==i+1 && b==j+1)){
						continue;
					}
					if(isInBounds(a, b) && grid.getSquare(a,b).getNeighborBombs()-grid.getSquare(a,b).getIdentifiedNeighborMines() == 2 && grid.getSquare(a,b).getHiddenSquares() == 3){
						if((i-a)!=0){	
							if(isInBounds(i, j-1) && grid.getSquare(i,j-1).isRevealed()){
								if(isInBounds(i-2*(i-a),j+1) && !grid.getSquare(i-2*(i-a),j+1).isRevealed()){
									if(!grid.getSquare(a,b+1).isRevealed() && !grid.getSquare(i,j+1).isRevealed()){
										System.out.println("a 1-2 found thanks to "+a+" "+b+", flagging "+(i-2*(i-a))+" "+(j+1));
										grid.getSquare(i-2*(i-a),j+1).reveal();
										grid.getSquare(i-2*(i-a),j+1).setFlagged(true);
										grid.print();
										return true;
									}
								}
							}							
							else{
								if(isInBounds(i-2*(i-a),j-1) && !grid.getSquare(i-2*(i-a),j-1).isRevealed()){
									if(!grid.getSquare(a,b-1).isRevealed() && !grid.getSquare(i,j-1).isRevealed()){
										System.out.println("b 1-2 found thanks to "+a+" "+b+", flagging "+(i-2*(i-a))+" "+(j-1));
										grid.getSquare(i-2*(i-a),j-1).reveal();
										grid.getSquare(i-2*(i-a),j-1).setFlagged(true);
										grid.print();
										return true;
									}
								}
							}
						}
						else{
							if(isInBounds(i-1, j) && grid.getSquare(i-1,j).isRevealed()){
								if(isInBounds(i+1,j-2*(j-b)) && !grid.getSquare(i+1,j-2*(j-b)).isRevealed()){
									if(!grid.getSquare(a+1,b).isRevealed() && !grid.getSquare(i+1,j).isRevealed()){
										System.out.println("c 1-2 found thanks to "+a+" "+b+", flagging "+(i+1)+" "+(j-2*(j-b)));
										grid.getSquare(i+1,j-2*(j-b)).reveal();
										grid.getSquare(i+1,j-2*(j-b)).setFlagged(true);
										grid.print();
										return true;
									}
								}
							}
							else{
								if(isInBounds(i-1,j-2*(j-b)) && !grid.getSquare(i-1,j-2*(j-b)).isRevealed()){
									if(!grid.getSquare(a-1,b).isRevealed() && !grid.getSquare(i-1,j).isRevealed()){
										System.out.println("d 1-2 found thanks to "+a+" "+b+", flagging "+(i-1)+" "+(j-2*(j-b)));
										grid.getSquare(i-1,j-2*(j-b)).reveal();
										grid.getSquare(i-1,j-2*(j-b)).setFlagged(true);
										grid.print();
										return true;
									}
								}
							}
						}						
					}
				}
			}
		}
		return false;
	}
	public void solve() {
		while(!grid.isSolved()) {
			boolean breakflag = false;
			int clear = size*size;
			//This next block will keep updating the knowledge base the knowledge base doesn't change
			//This is because if you add some knowledge, it could update another part that we already iterated through			
			
			//first, update all squares
			for (int i=0;i<size;i++) {
				for (int j=0;j<size;j++) {
					updateBase(grid.getSquare(i, j));	
				}
			}
			//next, check if all squares have been revealed; if so, game is done
			for(int i=0;i<size;i++){
				for(int j=0;j<size;j++){
					if(grid.getSquare(i,j).isRevealed()){
						clear--;
					}
				}
			}
			if(clear==0){
				int count=0;
				for(int i=0;i<size;i++) {
					for (int j=0;j<size;j++) {
						if (grid.getSquare(i,j).isFlagged()) {
							count++;
						}
					}
				}
				System.out.println("Final Score: " + count + "/" + grid.getNumBomb());
				grid.setSolved(true);
				break;
			}
			//if queue has any safeguesses, reveal those squares and update
			// while (!queue.isEmpty()) {
			// 	Square safeGuess = queue.remove();
			// 	grid.revealSquare(safeGuess.getX(), safeGuess.getY());
			// }
			// for (int i=0;i<size;i++) {
			// 	for (int j=0;j<size;j++) {
			// 		updateBase(grid.getSquare(i, j));	
			// 	}
			// }	
			//once every square has update information, attempt to find new information (flag or add safeguess squares)
			for (int i=0;i<size;i++){
				for(int j=0;j<size;j++){
					if(attempt1(grid.getSquare(i,j))){
						breakflag = true;
						break;
					}
				}
				if(breakflag)
				{break;}
			}
			if(!breakflag){
				System.out.println("!Attempt1");
				for (int i=0;i<size;i++){
					for(int j=0;j<size;j++){
						if(attempt2(grid.getSquare(i,j))){
							breakflag = true;
							break;
						}
					}
					if(breakflag)
					{break;}
				}
			}
			if(!breakflag){
				// we should update probabilities, and then choose the lowest probability (safest) to be x and y, and then continue
				System.out.println("!Attempt2, Guessing");
				int randX = (int)(Math.random()*size);
	  	      	int randY = (int)(Math.random()*size);
				while (!grid.revealSquare(randX, randY)) {
					randX = (int)(Math.random()*size);
			        randY = (int)(Math.random()*size);
				}
				updateBase(grid.getSquare(randX, randY));
				grid.print();
			}
		}
	}
	public void updateProbability(Grid grid) {
		//we are looking to update the probability of each square if possible
		for(int i=0; i<grid.getSize(); i++) {
			for(int j=0; j<grid.getSize(); j++) {
				//if NO NEIGHBOR SQUARES REVEALED (all 8 neighbors are hidden squares), then probability is 1/2
				if(grid.getSquare(i,j).getHiddenSquares() == 8) {
					grid.getSquare(i,j).setProbability(1/2);
				}
				//if 1 NEIGHBOR SQUARE REVEALED, then probability is neighbor's remaining bombs divided by neighbor's revealed squares  
				if(grid.getSquare(i,j).getHiddenSquares() == 7) {
					int x=0;
					int y=0;
					//we must first identify the neighboring square to get it's number of remaining bombs
					for(int a=i-1;a<i+2;a++){
						for(int b=j-1;b<j+2;b++){
							if(isInBounds(a,b) && (grid.getSquare(a,b).isRevealed()) ) {
								x=a;
								y=b;
							}
							
						}
					}
					//set probability
					grid.getSquare(i,j).setProbability( ((grid.getSquare(x,y).getNeighborBombs()-grid.getSquare(x,y).getIdentifiedNeighborMines())/(8-grid.getSquare(x,y).getHiddenSquares())) );
				}
			}
		}
	}
}