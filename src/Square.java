
public class Square {
	private boolean isBomb;
	private boolean revealed;
	private int neighborBombs;
	private boolean flagged;
	private int x;
	private int y;
	private int safeSquares;
	private int identifiedNeighborMines;
	private int identifiedSafeSquares;
	private int hiddenSquares;
	private double probability;

	public Square(boolean isBomb, int x, int y) {
		this.isBomb = isBomb;
		this.x = x;
		this.y = y;
	}
	public boolean isBomb() {
		return isBomb;
	}
	public void setBomb(boolean isBomb) {
		this.isBomb = isBomb;
	}
	public boolean reveal() {
		revealed = true;
		return isBomb;
	}
	public boolean isRevealed() {
		return revealed;
	}
	public int getNeighborBombs() {
		return neighborBombs;
	}
	public void setNeighborBombs(int neighborBombs) {
		this.neighborBombs = neighborBombs;
	}
	public boolean isFlagged() {
		return flagged;
	}
	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getSafeSquares() {
		return safeSquares;
	}
	public void setSafeSquares(int safeSquares) {
		this.safeSquares = safeSquares;
	}
	public int getIdentifiedSafeSquares() {
		return identifiedSafeSquares;
	}
	public void setIdentifiedSafeSquares(int identifiedSafeSquares) {
		this.identifiedSafeSquares = identifiedSafeSquares;
	}
	public int getIdentifiedNeighborMines() {
		return identifiedNeighborMines;
	}
	public void setIdentifiedNeighborMines(int identifiedNeighborMines) {
		this.identifiedNeighborMines = identifiedNeighborMines;
	}
	public int getHiddenSquares() {
		return hiddenSquares;
	}
	public void setHiddenSquares(int hiddenSquares) {
		this.hiddenSquares = hiddenSquares;
	}
	public double getProbability() {
		return probability;
	}
	public void setProbability(double probability) {
		this.probability = probability;
	}
}
